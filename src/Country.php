<?php

/*
 * Copyright (C) 2017 James Pole <james@pole.net.nz>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace CellSites\WebBuilder;

use JsonSerializable;

class Country extends NamedObject implements JsonSerializable
{
    private $networks = array();

    public function __construct($id, $name)
    {
        parent::__construct($id, $name);
    }
    public function addNetwork(Network $network)
    {
        $this->networks[$network->getID()] = $network;
    }
    public function getNetwork($id)
    {
        if (isset($this->networks[$id]) !== true) {
            return null;
        }
        return $this->networks[$id];
    }
    public function getNetworks()
    {
        return $this->networks;
    }
    public function jsonSerialize()
    {
        $array = parent::jsonSerialize();
        $array['type'] = 'Country';
        return $array;
    }
}

