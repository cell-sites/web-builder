<?php

/*
 * Copyright (C) 2017 James Pole <james@pole.net.nz>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace CellSites\WebBuilder;

class Dumper
{
    private $database = null;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }
    public function dump()
    {
        $string = '';
        foreach ($this->database->getCountries() as $country) {
            $string .= sprintf('Country %d: %s' . PHP_EOL, $country->getID(), $country);
            $string .= self::dumpNetworks($country);
        }
        return $string;
    }
    private static function dumpNetworkLocations(Network $network)
    {
        $string = '';
        foreach ($network->getNetworkLocations() as $thisNetworkLocation)
        {
            $location = $thisNetworkLocation->getLocation();
            $string .= sprintf(
                'Location: %d: %s [%s,%s] (%s)' . PHP_EOL,
                $location->getID(),
                $location,
                $location->getLatitude(),
                $location->getLongitude(),
                $thisNetworkLocation->getNetworkReference()
            );
        }
        return $string;
    }
    private static function dumpNetworks(Country $country)
    {
        $string = '';
        foreach ($country->getNetworks() as $network) {
            $string .= sprintf('Network %s: %s' . PHP_EOL, $network->getFullID(), $network);
            $string .= self::dumpNetworkLocations($network);
        }
        return $string;
    }
}

