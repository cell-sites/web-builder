<?php

/*
 * Copyright (C) 2017 James Pole <james@pole.net.nz>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace CellSites\WebBuilder;

use RuntimeException;
use SplFileInfo;

class CsvDatabase extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->readGlobalLocations();
        $this->readCountries();
    }
    private function readCountries()
    {
        foreach (new CsvFile('mcc.csv') as $row) {
            $country = new Country($row[0], $row[1]);
            $this->addCountry($country);
            $this->readNetworks($country);
        }
    }
    private function readGlobalLocations()
    {
        foreach (new CsvFile('location.csv') as $row) {
            if (count($row) < 4) {
                continue;
            }
            $this->getLocations()->addLocation(new Location($row[0], $row[1], $row[2], $row[3]));
        }
    }
    private function readGsmCells(Network $network)
    {
        if (($network->getStandards() & Network::STANDARD_GSM) === 0) {
            return;
        }
        $filename = sprintf(
            'mcc-%d/mnc-%d/cell-gsm.csv',
            $network->getCountry()->getID(),
            $network->getID()
        );
        foreach (new CsvFile($filename) as $row) {
            new GsmCell(
                $network,
                $row[0],
                isset($row[2]) ? $network->getNetworkLocation($row[2]) : null,
                $row[1]
            );
        }
    }
    private function readNetworkLocations(Network $network)
    {
        $filename = sprintf(
            'mcc-%d/mnc-%d/location.csv',
            $network->getCountry()->getID(),
            $network->getID()
        );
        foreach (new CsvFile($filename) as $row) {
            if (isset($row[1]) !== true) {
                throw new RuntimeException('Network Location did not have a network reference. Location was ' . $row[0] . ' and network was ' . $network->getFullID() . '.');
            }
            new NetworkLocation(
                $network,
                $this->getLocations()->getLocation($row[0]),
                isset($row[1]) ? $row[1] : null
            );
        }
    }
    private function readNetworks(Country $country)
    {
        $filename = sprintf('mcc-%d/mnc.csv', $country->getID());
        foreach (new CsvFile($filename) as $row) {
            $network = new Network($country, $row[0], $row[1], $row[2]);
            $this->readNetworkLocations($network);
            $this->readGsmCells($network);
            $this->readUmtsCells($network);
        }
    }
    private function readUmtsCells(Network $network)
    {
        if (($network->getStandards() & Network::STANDARD_UMTS) === 0) {
            return;
        }
        $filename = sprintf(
            'mcc-%d/mnc-%d/cell-umts.csv',
            $network->getCountry()->getID(),
            $network->getID()
        );
        foreach (new CsvFile($filename) as $row) {
            new UmtsCell(
                $network,
                $row[0],
                isset($row[3]) ? $network->getNetworkLocation($row[3]) : null,
                $row[1],
                $row[2]
            );
        }
    }
}

