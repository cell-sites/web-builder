<?php

/*
 * Copyright (C) 2017 James Pole <james@pole.net.nz>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace CellSites\WebBuilder;

use JsonSerializable;

class Network extends NamedObject implements JsonSerializable
{
    const STANDARD_GSM  = 1;
    const STANDARD_UMTS = 2;
    const STANDARD_LTE  = 4;

    private $cells = array();
    private $country = null;
    private $networkLocations = array();
    private $standards = null;

    public function __construct(Country $country, $id, $name, $standards)
    {
        parent::__construct($id, $name);
        $this->country = $country;
        $this->country->addNetwork($this);
        $this->standards = $standards;
    }
    public function addCell(Cell $cell)
    {
        array_push($this->cells, $cell);
    }
    public function addNetworkLocation(NetworkLocation $networkLocation)
    {
        $this->networkLocations[$networkLocation->getLocation()->getID()] = $networkLocation;
    }
    public function getCells()
    {
        return $this->cells;
    }
    public function getCountry()
    {
        return $this->country;
    }
    public function getFullID()
    {
        return sprintf('%03d-%02d', $this->getCountry()->getID(), $this->getID());
    }
    public function getNetworkLocation($id)
    {
        if (isset($this->networkLocations[$id]) !== true) {
            return null;
        }
        return $this->networkLocations[$id];
    }
    public function getNetworkLocations()
    {
        return $this->networkLocations;
    }
    public function getStandards()
    {
        return $this->standards;
    }
    public function getUmtsCells()
    {
        $umtsCells = array();
        foreach ($this->getCells() as $thisCell) {
            if ($thisCell instanceof UmtsCell) {
                array_push($umtsCells, $thisCell);
            }
        }
        return $umtsCells;
    }
    public function jsonSerialize()
    {
        $array = parent::jsonSerialize();
        $array['country'] = $this->getCountry()->jsonSerialize();
        $array['type'] = 'Network';
        return $array;
    }
}

