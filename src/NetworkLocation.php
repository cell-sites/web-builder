<?php

/*
 * Copyright (C) 2017 James Pole <james@pole.net.nz>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace CellSites\WebBuilder;

use JsonSerializable;

class NetworkLocation implements JsonSerializable
{
    private $cells = array();
    private $location = null;
    private $network = null;
    private $networkReference = null;

    public function __construct(
        Network $network,
        Location $location,
        $networkReference = null
    ) {
        $this->network = $network;
        $this->location = $location;
        $this->networkReference = $networkReference;
        $this->network->addNetworkLocation($this);
        $this->location->addNetworkLocation($this);
    }
    public function addCell(Cell $cell)
    {
        array_push($this->cells, $cell);
    }
    public function getCells()
    {
        return $this->cells;
    }
    public function getLocation()
    {
        return $this->location;
    }
    public function getNetwork()
    {
        return $this->network;
    }
    public function getNetworkReference()
    {
        return $this->networkReference;
    }
    public function jsonSerialize()
    {
        $array = array();
        $array['type'] = 'NetworkLocation';
        $array['network'] = $this->getNetwork()->jsonSerialize();
        $array['networkReference'] = $this->getNetworkReference();
        return $array;
    }
}

