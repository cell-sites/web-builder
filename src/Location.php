<?php

/*
 * Copyright (C) 2017 James Pole <james@pole.net.nz>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace CellSites\WebBuilder;

use JsonSerializable;

class Location extends NamedObject implements JsonSerializable
{
    private $latitude = null;
    private $longitude = null;
    private $networkLocations = array();

    public function __construct($id, $name, $latitude, $longitude)
    {
        parent::__construct($id, $name);
        $this->latitude = (double)$latitude;
        $this->longitude = (double)$longitude;
    }
    public function addNetworkLocation(NetworkLocation $networkLocation)
    {
        $this->networkLocations[$networkLocation->getNetwork()->getFullID()] = $networkLocation;
    }
    public function getLatitude()
    {
        return $this->latitude;
    }
    public function getLongitude()
    {
        return $this->longitude;
    }
    public function jsonSerialize() {
        $object['type'] = 'Feature';
        $object['id'] = $this->getID();
        $object['geometry']['type'] = 'Point';
        $object['geometry']['coordinates'] = array($this->longitude, $this->latitude);
        $object['properties']['name'] = (string)$this;
        $object['properties']['networkLocations'] = array();
        foreach ($this->networkLocations as $thisNetworkLocation) {
            $object['properties']['networkLocations'][] = $thisNetworkLocation->jsonSerialize();
        }
        return $object;
    }
}

