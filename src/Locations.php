<?php

/*
 * Copyright (C) 2017 James Pole <james@pole.net.nz>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace CellSites\WebBuilder;

use JsonSerializable;
use RuntimeException;

class Locations implements JsonSerializable
{
    private $locations = null;

    public function addLocation(Location $location)
    {
        $this->locations[$location->getID()] = $location;
    }
    public function getLocation($id) {
        if (isset($this->locations[$id]) !== true) {
            throw new RuntimeException('Location with the given ID was not found.');
        }
        return $this->locations[$id];
    }
    public function jsonSerialize() {
        $object['type'] = 'FeatureCollection';
        $object['features'] = array();
        foreach ($this->locations as $thisLocation) {
            $object['features'][] = $thisLocation;
        }
        return $object;
    }
}

