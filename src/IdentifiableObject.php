<?php

/*
 * Copyright (C) 2017 James Pole <james@pole.net.nz>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace CellSites\WebBuilder;

use JsonSerializable;

abstract class IdentifiableObject implements JsonSerializable
{
    private $id = null;

    protected function __construct($id)
    {
        $this->id = $id;
    }
    public function getID()
    {
        return $this->id;
    }
    public function jsonSerialize()
    {
        $array = array();
        $array['id'] = $this->getID();
        return $array;
    }
}

